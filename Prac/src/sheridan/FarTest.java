package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FarTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testcFc() {
		int fe = Far.cFc(20);
		assertTrue("ryt", fe==68);
	}

	@Test
	public void testcFcNegative() {
		int fe = Far.cFc(21);
		assertFalse("ryt", fe==68);
	}
	@Test
	public void testcFcBoundryIn() {
		int fe = Far.cFc(32);
		assertTrue("ryt", fe==89);
	}
	@Test
	public void testcFcBoundryOut() {
		int fe = Far.cFc(32);
		assertFalse("ryt", fe==89.6);
	}
}
